include:
    # Python black and PEP8 code style check
    - "https://gitlab.com/nobodyinperson/ci-templates/raw/master/python-black-pep8.yml"
    # PyPI upload
    - "https://gitlab.com/nobodyinperson/ci-templates/raw/master/pypi-upload.yml"

image: python:latest

variables:
    SPHINX_DOC_DIR: sphinx-doc
    COVERAGE_STATIC_DIR: coverage-report
    PAGES_DIR: public

stages:
    - test
    - package
    - deploy

cache:
    key: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"

coverage:python:latest:
    stage: test
    image: python:latest
    script:
        - pip3 install coveralls
        - "PACKAGE_NAME=`python3 -c 'from setuptools import find_packages;print(find_packages(exclude=[\"tests\"])[0])'`"
        - coverage run --source=$PACKAGE_NAME setup.py test
        - coverage html
        - coverage report
        - mv htmlcov/ $COVERAGE_STATIC_DIR
    coverage: '/TOTAL.*\s+(\d+\%)/'
    artifacts:
        paths:
            - $COVERAGE_STATIC_DIR
        expire_in: 1 week

coverage:python:3.6:
    stage: test
    image: python:3.6
    script:
        - pip3 install coveralls
        - "PACKAGE_NAME=`python3 -c 'from setuptools import find_packages;print(find_packages(exclude=[\"tests\"])[0])'`"
        - coverage run --source=$PACKAGE_NAME setup.py test
        - coverage report

coverage:python:3.5:
    stage: test
    image: python:3.5
    script:
        - pip3 install coveralls
        - "PACKAGE_NAME=`python3 -c 'from setuptools import find_packages;print(find_packages(exclude=[\"tests\"])[0])'`"
        - coverage run --source=$PACKAGE_NAME setup.py test
        - coverage report

coverage:python:3.4:
    stage: test
    image: python:3.4
    variables:
        MPLBACKEND: agg
    script:
        - pip3 install . --only-binary=matplotlib,numpy,scipy
        - pip3 install coveralls
        - "PACKAGE_NAME=`python3 -c 'from setuptools import find_packages;print(find_packages(exclude=[\"tests\"])[0])'`"
        - coverage run --source=$PACKAGE_NAME setup.py test
        - coverage report

python-black-pep8:
    stage: test

dist:
    stage: package
    dependencies: [] # no other artifacts needed
    script:
        - ./setup.py sdist
    artifacts:
        paths:
            - dist/*
        expire_in: 1 week

sphinx:
    image: ubuntu
    stage: package
    dependencies: [] # no other artifacts needed
    script:
        - "apt-get -qq update 2>&1 >/dev/null"
        - "apt-get -y -qq install pandoc python3-pip >/dev/null"
        - pip3 install --user .
        - pip3 install sphinx sphinx_rtd_theme sphinxcontrib-mermaid jupyter ipython
        - make docs
        - mv docs/_build/html $SPHINX_DOC_DIR
    artifacts:
        paths:
            - $SPHINX_DOC_DIR
        expire_in: 1 week

pypi-upload:
    stage: deploy
    environment:
        name: Python Package Index
        url: https://pypi.org/project/class-tools/
    dependencies:
        - dist
    only:
        - master
    only:
        - tags

pages:
    image: alpine
    variables:
        GIT_STRATEGY: none
    before_script: [] # no need to install anything for deployment
    stage: deploy
    environment:
        name: GitLab Pages
        url: https://nobodyinperson.gitlab.io/python3-class-tools
    dependencies:
        - sphinx
        - coverage:python:latest
    script:
        - rm -rf $PAGES_DIR/ # make sure there is no pages dir
        - mv $SPHINX_DOC_DIR $PAGES_DIR/ # sphinx doc is index page
        - mv $COVERAGE_STATIC_DIR $PAGES_DIR/ # put coverage report inside
    artifacts:
        paths:
            - $PAGES_DIR/
    only:
        - master
    only:
        - tags

