
Installation
============

:mod:`class-tools` is best installed via :mod:`pip`::

    python3 -m pip install --user class-tools
