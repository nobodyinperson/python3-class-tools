Welcome to class-tools's documentation!
===========================================

:mod:`class-tools` is a collection of Python class utilities that the author
frequently needs.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   api/modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
