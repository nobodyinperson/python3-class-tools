#!/usr/bin/env make -f

SETUP.PY = ./setup.py
PACKAGE_FOLDER = $(shell python3 -c 'from setuptools import find_packages;print(find_packages(exclude=["tests"])[0])')
DOCS_FOLDER = docs
DOCS_API_FOLDER = docs/source/api
VERSION.PY = $(shell find $(PACKAGE_FOLDER) -type f -name 'version.py')

# get version from __init__.py
VERSION = $(shell perl -ne 'if (s/^.*__version__\s*=\s*"(\d+\.\d+.\d+)".*$$/$$1/g){print;exit}' $(VERSION.PY))

.PHONY: all
all: coverage docs locale

.PHONY: docs
docs:
	cd $(DOCS_FOLDER) && make html
	-xdg-open $(DOCS_FOLDER)/_build/html/index.html

.PHONY: coverage
coverage:
	coverage run --source=$(PACKAGE_FOLDER) $(SETUP.PY) test
	coverage report
	coverage html
	-xdg-open htmlcov/index.html

.PHONY: clean
clean:
	rm -rf *.egg-info
	rm -rf build
	rm -rf $$(find -type d -iname '__pycache__')
	rm -f $$(find -type f -iname '*.pyc')
	rm -f $(CHANGELOG_RPM)
	(cd $(DOCS_FOLDER) && make clean)
