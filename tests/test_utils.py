# system modules
import unittest
import itertools

# internal modules
from class_tools.utils import *

# external modules


class UtilsTest(unittest.TestCase):
    def test_full_object_path(self):
        for obj, path in (
            (full_object_path, "class_tools.utils.full_object_path"),
            (str, "str"),
        ):
            with self.subTest(object=obj):
                self.assertEqual(full_object_path(obj), path)

    def test_get_properties(self):
        class TestClass:
            prop = property

        kw = {
            "fget": lambda x: 1,
            "fset": lambda x, n: None,
            "fdel": lambda x: None,
        }

        # fill class with all kinds of properties
        for comb in itertools.chain.from_iterable(
            itertools.combinations(kw, i) for i in range(1, len(kw) + 1)
        ):
            setattr(
                TestClass,
                "prop" + ("".join(comb)),
                property(**{k: v for k, v in kw.items() if k in comb}),
            )

        obj = TestClass()
        trans = {"fget": "getter", "fset": "setter", "fdel": "deleter"}
        # tests
        for b in (True, False):
            for comb in itertools.chain(
                (tuple(),),
                itertools.chain.from_iterable(
                    itertools.combinations(kw, i)
                    for i in range(1, len(kw) + 1)
                ),
            ):
                props = get_properties(obj, **{trans[k]: b for k in comb})
                for name, prop in props.items():
                    for attr in comb:
                        if b:
                            self.assertTrue(
                                getattr(prop, attr),
                                "{} property has no {}".format(
                                    name, trans[attr]
                                ),
                            )
                        else:
                            self.assertFalse(
                                getattr(prop, attr),
                                "{} property unexpectedly has a {}".format(
                                    name, trans[attr]
                                ),
                            )
