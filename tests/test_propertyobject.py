# system modules
import unittest

# internal modules
import class_tools
from class_tools.propertyobject import PropertyObject
from class_tools.decorators import *

# external modules


class PropertyObjectTest(unittest.TestCase):
    def test_repr_consistency_bare(self):
        obj = PropertyObject()
        self.assertEqual(eval(repr(obj)), obj)

    def test_repr_consistency(self):
        @wrapper_property("prop1", static_default=1)
        @wrapper_property("prop2", static_default=2)
        @wrapper_property("no_setter", static_default=2, fset=None)
        @constant("constant", 1234)
        class TestClass(PropertyObject):
            pass

        obj = TestClass()
        self.assertNotIn("constant", repr(obj))
        self.assertNotIn("no_deleter", repr(obj))
        self.assertEqual(eval(repr(obj)), obj)
