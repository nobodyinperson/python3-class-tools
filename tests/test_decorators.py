# system modules
import unittest
import itertools

# internal modules
from class_tools.decorators import *

# external modules


class ClassDecoratorTest(unittest.TestCase):
    def test_classdecorator_raises_for_function(self):
        @classdecorator
        def function_decorator(fun):
            return fun

        with self.assertRaises(TypeError):

            @function_decorator
            def func():
                pass

    def test_classdecorator_works_for_classes(self):
        @classdecorator
        def decorator(cls):
            cls.attr = True
            return cls

        @decorator
        class TestClass:
            pass

        self.assertIsInstance(TestClass, type)
        self.assertIs(TestClass.attr, True)


class ConflictingArgumentsDecoratorTest(unittest.TestCase):
    def test_decorator(self):
        @conflicting_arguments("a", "b")
        def fun(a, *args, b="b", c="c", **kwargs):
            return "".join((a, b, c))

        self.assertEqual(fun("A"), "Abc")
        self.assertEqual(fun("A", c="C", something=1), "AbC")
        with self.assertRaises(ValueError):
            fun("A", "B")
        with self.assertRaises(ValueError):
            fun("A", b="B")
        with self.assertRaises(ValueError):
            fun(a="A", b="B")


class AddPropertyDecoratorTest(unittest.TestCase):
    def test_decorator(self):
        @add_property(
            "prop",
            lambda s: s._prop,
            lambda s, n: setattr(s, "_prop", n),
            lambda s: delattr(s, "_prop"),
        )
        class TestClass:
            pass

        t = TestClass()
        self.assertFalse(hasattr(t, "_prop"))
        t.prop = 1
        self.assertTrue(hasattr(t, "_prop"))
        self.assertEqual(t.prop, 1)
        del t.prop
        self.assertFalse(hasattr(t, "_prop"))


class ReadOnlyPropertyDecoratorTest(unittest.TestCase):
    def test_decorator(self):
        @readonly_property("prop", getter=lambda x: [])
        class TestClass:
            pass

        t1 = TestClass()
        t2 = TestClass()
        self.assertIsNot(t1.prop, t2.prop)
        with self.assertRaises(AttributeError):
            t1.prop = 1
        with self.assertRaises(AttributeError):
            del t1.prop


class ConstantDecoratorTest(unittest.TestCase):
    def test_decorator(self):
        @constant("prop", [])
        class TestClass:
            pass

        t1 = TestClass()
        t2 = TestClass()
        self.assertIs(t1.prop, t2.prop)
        with self.assertRaises(AttributeError):
            t1.prop = 1
        with self.assertRaises(AttributeError):
            del t1.prop


class WrapperPropertyDecoratorTest(unittest.TestCase):
    def test_decorator(self):
        @wrapper_property("prop", attr="_prop")
        class TestClass:
            pass

        t = TestClass()
        self.assertFalse(hasattr(t, "_prop"))
        t.prop = 1
        self.assertTrue(hasattr(t, "_prop"))
        self.assertEqual(t.prop, 1)
        del t.prop
        self.assertFalse(hasattr(t, "_prop"))

    def test_creates_property(self):
        for name in ("prop", "other", "__prop"):
            with self.subTest(name=name):

                @wrapper_property(name)
                class TestClass:
                    pass

                self.assertTrue(hasattr(TestClass, name))
                self.assertIsInstance(getattr(TestClass, name), property)
                obj = TestClass()

    def test_static_default(self):
        for name, static_default, set_default in itertools.product(
            ("prop", "otherprop"), (5, "asdf", None, str), (True, False)
        ):
            with self.subTest(
                name=name,
                static_default=static_default,
                set_default=set_default,
            ):

                @wrapper_property(
                    name,
                    static_default=static_default,
                    set_default=set_default,
                )
                class TestClass:
                    pass

                attr = "".join(("_", name))
                obj = TestClass()
                self.assertFalse(
                    hasattr(obj, attr),
                    "the underlying attribute is already set "
                    "without the getter being called",
                )
                v = getattr(obj, name)
                self.assertIs(
                    v,
                    static_default,
                    "Instead of the default value {}, "
                    "the property returns {}".format(
                        repr(static_default), repr(v)
                    ),
                )
                (self.assertTrue if set_default else self.assertFalse)(
                    hasattr(obj, attr),
                    "despite set_default={}, "
                    "the underlying attribute {} set after "
                    "accessing the property".format(
                        set_default, "is not" if set_default else "is"
                    ),
                )

    def test_dynamic_default(self):
        for name, dynamic_default, set_default in itertools.product(
            ("prop", "otherprop"),
            (lambda x: list(), lambda x: x),
            (True, False),
        ):
            with self.subTest(
                name=name,
                dynamic_default=dynamic_default,
                set_default=set_default,
            ):

                @wrapper_property(
                    name,
                    dynamic_default=dynamic_default,
                    set_default=set_default,
                )
                class TestClass:
                    pass

                attr = "".join(("_", name))
                obj = TestClass()
                self.assertFalse(
                    hasattr(obj, attr),
                    "the underlying attribute is already set "
                    "without the getter being called",
                )
                v = getattr(obj, name)
                if isinstance(v, list):
                    self.assertIsNot(
                        v,
                        dynamic_default(obj),
                        "dynamic_default list is not per-object",
                    ),
                else:
                    self.assertIs(
                        v,
                        dynamic_default(obj),
                        "dynamic_default does not work",
                    )
                (self.assertTrue if set_default else self.assertFalse)(
                    hasattr(obj, attr),
                    "despite set_default={}, "
                    "the underlying attribute {} set after "
                    "accessing the property".format(
                        set_default, "is not" if set_default else "is"
                    ),
                )

    def test_setter_deleter(self):
        for (
            name,
            static_type,
            dynamic_type,
            value,
            static,
        ) in itertools.product(
            ("prop", "_otherprop"),
            (NotSet, str, float),
            (NotSet, lambda x, v: str(v), lambda x, v: float(v)),
            ("1", "1.0", 3, 3.14),
            (True, False),
        ):
            with self.subTest(
                name=name,
                static_type=static_type,
                dynamic_type=dynamic_type,
                value=value,
            ):

                if static:

                    @wrapper_property(name, static_type=static_type)
                    class TestClass:
                        pass

                else:

                    @wrapper_property(name, dynamic_type=dynamic_type)
                    class TestClass:
                        pass

                attr = "".join(("_", name))
                obj = TestClass()
                if static:
                    conv = (
                        (lambda x: x) if static_type is NotSet else static_type
                    )(value)
                else:
                    conv = (
                        (lambda x, v: v)
                        if dynamic_type is NotSet
                        else dynamic_type
                    )(obj, value)
                setattr(obj, name, value)
                v = getattr(obj, name)
                self.assertEqual(
                    v,
                    conv,
                    "Instead of {} the getter returns {} after setting "
                    "the property to {}".format(conv, v, value),
                )
                av = getattr(obj, attr)
                self.assertEqual(
                    av,
                    v,
                    "Instead of {} the underlying attribute is {} "
                    "after setting the property to {}".format(v, av, value),
                )
                delattr(obj, name)
                self.assertFalse(
                    hasattr(obj, attr),
                    "Deleting the property didn't delete "
                    "the underlying attribute",
                )


class InitFromPropertiesDecoratorTest(unittest.TestCase):
    def test_init_sets_properties(self):
        propnames = ("prop", "other", "__other")
        for names in itertools.chain.from_iterable(
            itertools.combinations(propnames, i)
            for i in range(2, len(propnames) + 1)
        ):
            with self.subTest(properties=names):

                @with_init_from_properties()
                class TestClass:
                    pass

                for name in names:
                    TestClass = wrapper_property(name)(TestClass)
                props = dict(map(reversed, enumerate(names)))
                obj = TestClass(**props)
                for name, val in props.items():
                    v = getattr(obj, name)
                    self.assertEqual(
                        v,
                        val,
                        "Despite specifying {name}={val} in the initializer "
                        "the property {name} is {propval} "
                        "instead of {val}".format(
                            name=name, val=val, propval=v
                        ),
                    )

    def test_init_warns_for_extra_arguments(self):
        propnames = ("prop", "other", "__other")
        for names in itertools.chain.from_iterable(
            itertools.combinations(propnames, i)
            for i in range(2, len(propnames) + 1)
        ):
            with self.subTest(properties=names):

                @with_init_from_properties()
                class TestClass:
                    pass

                for name in names:
                    TestClass = wrapper_property(name)(TestClass)
                props = dict(map(reversed, enumerate(names)))
                props["".join(props)] = "asdf"
                with self.assertWarnsRegex(
                    UserWarning, r"unexpected.*argument"
                ):
                    TestClass(**props)


class EqComparingPropertiesDecoratorTest(unittest.TestCase):
    def test_eq(self):
        propnames = ("prop", "other", "__other")
        for names, equal in itertools.product(
            itertools.chain.from_iterable(
                itertools.combinations(propnames, i)
                for i in range(1, len(propnames) + 1)
            ),
            (True, False),
        ):
            with self.subTest(properties=names, equal=equal):

                @with_eq_comparing_properties()
                class TestClass:
                    pass

                for name in names:
                    TestClass = wrapper_property(name)(TestClass)
                props = dict(map(reversed, enumerate(names)))
                obj1 = TestClass()
                obj2 = TestClass()
                for name, val in props.items():
                    setattr(obj1, name, val)
                    setattr(obj2, name, val if equal else (val + 1))
                (self.assertTrue if equal else self.assertFalse)(
                    obj1 == obj2,
                    "Despite having {equal_props} properties, "
                    "__eq__ reports that the "
                    "two objects are {comparison}".format(
                        equal_props="equal" if equal else "different",
                        comparison="different" if equal else "equal",
                    ),
                )

    def test_eq_raises_typeerror_if_not_same_properties(self):
        propnames = ("prop", "other", "__other")
        for names in itertools.chain.from_iterable(
            itertools.combinations(propnames, i)
            for i in range(1, len(propnames) + 1)
        ):
            with self.subTest(properties=names):

                @with_eq_comparing_properties()
                class TestClass:
                    pass

                for name in names:
                    TestClass = wrapper_property(name)(TestClass)

                class TestClass2:
                    pass

                obj1 = TestClass()
                obj2 = TestClass2()
                with self.assertRaisesRegex(TypeError, r"has\s+no\s+property"):
                    obj1 == obj2
